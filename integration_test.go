package main

import "testing"

func TestSimpleFailure_1(t *testing.T) {
	t.Errorf("Oh no I failed")
}

func TestSimpleSuccess_2(t *testing.T) {
	bla := 3
	if bla != 3 {
		t.Errorf("bla != 3 is %v", bla)
	}
}

func TestSkip_3(t *testing.T) {
	t.Skip("Skipping this test")
}