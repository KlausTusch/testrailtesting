# TestrailTesting

First up set up a testrail instance (can be done for free and supply the credentials as environment variables)
* `TESTRAIL_USERNAME` (email addresse)
* `TESTRAIL_PASSWORD` (API token or password)
* `TESTRAIL_SERVER` e.g.: https://strial.testrail.io

Look at the file integration_test.go, every test ends with a testCase id it should be mapped to. That means the testrail instance needs at least 3 test cases setup (ids: 1,2,3). 

Use `go test -json > test_results.json` to generate the new test results

Run the test program: `go run main.go` or `go run ./...`

This will create a new run called `Test Run from Go` add the test results to the run and archive it.

# Selenium 

The selenium sample is located in `selenium` and written with python, other programming languages can be used as well.

Supported selenium apis are available in the following languages:
* java
* python
* C#
* Ruby
* JavaScript

Testrail Supports the following apis officially:
* Java
* php
* python
* Ruby
* .Net

The project uses the geckodriver (firefox) which can be found here: https://github.com/mozilla/geckodriver/releases/tag/v0.29.1

## Setup

* Install selenium `pip install selenium`
* Install a webdriver for firefox ([docs](https://www.selenium.dev/documentation/en/webdriver/driver_requirements/))
* Install pytest `pip install pytest`
* Install the pytest-integration `pip install pytest-testrail`

## Run

Execute `cd selenium && py.test --testrail --tr-email <username> --tr-password <password> test.py` -> check run in testrail

# webdriver io

go to `webdriverio-react/test-app` execute `yarn install`and `npx wdio run ./wdio.conf.js` -> check run in testrail