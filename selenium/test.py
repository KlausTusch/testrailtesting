import unittest
import pytest

from selenium.webdriver import Firefox
from testrail import APIClient
from pytest_testrail.plugin import testrail

class TestSelenium(unittest.TestCase):

    # testrail caseID is supplied with an annotation
    @testrail('C26')
    def test_success_selenium(self):
        self.driver.get("https://www.google.com")
        self.assertEqual(self.driver.current_url, "https://www.google.com/")

    @testrail('C27')
    def test_failure_selenium(self):
        self.driver.get("https://www.wikipedia.org")
        self.assertEqual(self.driver.current_url, "https://www.google.com/")

    # setup and tear down the driver before and after the test runs
    @classmethod
    def setUpClass(self):
        self.driver = Firefox()

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()