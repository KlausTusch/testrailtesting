package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/educlos/testrail"
)

type goTestResult struct {
	TestName string `json:"Test"`
	Action string `json:"Action"`
	Output string `json:"Output"`

}


func main() {
	// open file
	file, err := os.Open("test_results.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	// setup testrailClient
	username := os.Getenv("TESTRAIL_USERNAME")
	password := os.Getenv("TESTRAIL_PASSWORD")
	server := os.Getenv("TESTRAIL_SERVER")
	projectID := 1
	caseIDs := []int{1,2,3}
	testrailClient := testrail.NewClient(server, username, password)

	// add a new testRun
	includeAll := false
	run, err := testrailClient.AddRun(projectID, testrail.SendableRun{Name: "Test Run from Go " + time.Now().Format(time.RFC3339), Description: "Some run", IncludeAll: &includeAll, CaseIDs: caseIDs})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//read go test results from file and get the testRail caseID from the testName in the result
	// the testname needs to end with a _id for this to work
	testMap := make(map[int][]goTestResult)
	for _,value := range unmarshalGoTestResultsFromFile(file) {
		if value.TestName != ""{
			caseID := getTestIDFromTestName(value.TestName)
			testMap[caseID] = append(testMap[caseID], value)
		}
	}

	// construct the sendable testResults for all testIDs contained in the map
	testrailResults := make([]testrail.ResultsForCase, 0)
	for caseID,value := range testMap {
		testrailResults = append(testrailResults, testrail.ResultsForCase{CaseID: caseID, SendableResult: constructTestRailResults(value)})
	}
	// send the results
	
	_, err = testrailClient.AddResultsForCases(run.ID, testrail.SendableResultsForCase{Results: testrailResults})
	//_, err = testrailClient.AddResults(run.ID, testrail.SendableResults{Results: testrailResults})
	if err != nil {
		fmt.Println(err)
	}

	// close the test run
	testrailClient.CloseRun(run.ID)
}

func getTestIDFromTestName(testName string) int {
	parts := strings.Split(testName, "_")
	caseID,_ := strconv.Atoi(parts[len(parts)-1]) 
	return caseID
}

// reads the input file line by line and unmarshals each line to a goTestResult, 
// returns the array of unmarshaled tests
func unmarshalGoTestResultsFromFile (file *os.File) []goTestResult {
	scanner := bufio.NewScanner(file)
	tests := make([]goTestResult, 0)
	for scanner.Scan() {
		var test goTestResult
		err := json.Unmarshal(scanner.Bytes(), &test)
		if err != nil {
			fmt.Println(err)
		}
		tests = append(tests, test)
	}
	return tests
}

// maps the goTestResults of a single test function to a sendable result 
// mapping resolve in the followin way:
//
// testrail.result.comment == []goTestResult.Output concated with \n
//
// testrail.result.status == gotestResult.Action
func constructTestRailResults (tests []goTestResult) (testrail.SendableResult) {
	var comment string
	var status int
	for _, test := range tests {
		switch test.Action {
			case "output":
				if !strings.HasPrefix(test.Output, "=== ") && !strings.HasPrefix(test.Output, "--- ") && !strings.HasPrefix(test.Output, "FAIL"){
					comment += test.Output
				}
			case "pass":
				// Passed
				status = 1
			case "fail":
				// Failed
				status = 5
			case "skip":
				// Blocked
				status = 2
			case "run":
				// ignore
			default:
				fmt.Println("unknown go result action", test.Action)
		}
	}
	return testrail.SendableResult{StatusID: status, Comment: comment}
}